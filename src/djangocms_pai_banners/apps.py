from django.apps import AppConfig


class DjangocmsPaiBannersConfig(AppConfig):
    name = 'djangocms_pai_banners'
